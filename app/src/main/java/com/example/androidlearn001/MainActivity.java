package com.example.androidlearn001;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    String msg = "Android: ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView = (TextView)findViewById(R.id.text001);
        textView.setText(R.string.hello);

        ImageView imageView = (ImageView)findViewById(R.id.img001);
        imageView.setImageResource(R.drawable.a);
        Log.d(msg,"The onCreate() event");

        findViewById(R.id.btn001).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ServiceActivity.class));
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d(msg,"The onStart() event");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(msg,"The onResume() event");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d(msg,"The onPause() event");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d(msg,"The onStop() event");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(msg,"The onDestroy() event");
    }

}
